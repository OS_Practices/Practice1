package files

import (
	"archive/zip"
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

type _zip struct {
	Files []string
}

func NewZip() *_zip {
	return &_zip{
		Files: make([]string, 0, 0),
	}
}

func (z *_zip) Create(path string) error {
	arch, err := os.Create(path)
	if err != nil {
		return err
	}
	defer arch.Close()

	writer := zip.NewWriter(arch)
	defer writer.Close()

	fmt.Println("Введите через запятую файлы, который надо положить в архив:")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	data := strings.Split(scanner.Text(), ",")

	for i := range data {
		z.Files = append(z.Files, data[i])

		file, err := os.Open(data[i])
		if err != nil {
			return err
		}
		defer file.Close()

		f, err := writer.Create(data[i])

		if err != nil {
			return err
		}

		if _, err = io.Copy(f, file); err != nil {
			return err
		}
	}

	return nil
}

func (z *_zip) Read(path string) error {
	arch, err := zip.OpenReader(path)
	if err != nil {
		return err
	}
	defer arch.Close()

	for i := range arch.File {
		fmt.Println("Файл: ", arch.File[i].Name)
		file, err := arch.File[i].Open()
		if err != nil {
			return err
		}
		defer file.Close()

		_, err = io.Copy(os.Stdout, file)

		if err != nil {
			return err
		}

		fmt.Println()
	}
	return nil
}

func (z *_zip) Update(path string) error {
	archReader, err := zip.OpenReader(path)
	if err != nil {
		return err
	}
	defer archReader.Close()

	arch, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		return err
	}
	defer arch.Close()

	archWriter := zip.NewWriter(arch)
	defer archWriter.Close()

	fmt.Println("Введите файлы, через запятую, которые необходимо добавить:")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	data := strings.Split(scanner.Text(), ",")

	for i := range data {
		file, err := os.Open(data[i])
		if err != nil {
			return nil
		}
		defer file.Close()

		fileInfo, err := file.Stat()
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(fileInfo)
		if err != nil {
			return err
		}

		header.Name = data[i]
		header.Method = zip.Deflate

		writer, err := archWriter.CreateHeader(header)
		if err != nil {
			return err
		}

		_, err = io.Copy(writer, file)

		if err != nil {
			return err
		}
	}

	return nil
}

func (z *_zip) Delete(path string) error {
	err := os.Remove(path)
	return err
}

func (z *_zip) String() {
	fmt.Println("Fio: ", z.Files)
}

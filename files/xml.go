package files

import (
	"bufio"
	"encoding/xml"
	"errors"
	"fmt"
	"os"
	"strings"
)

type _xml struct {
	Fio    string `xml:"fio"`
	Year   string `xml:"year"`
	Facult string `xml:"facult"`
}

func NewXml() *_xml {
	return &_xml{
		Fio:    "none",
		Year:   "none",
		Facult: "none",
	}
}

func (x *_xml) Create(path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	fmt.Println("Пожалуйста, введите информацию о ФИО, годе поступления и направлении студента, через запятую:")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	data := strings.Split(scanner.Text(), ",")

	x.Fio = data[0]
	x.Year = data[1]
	x.Facult = data[2]

	res, err := xml.Marshal(x)

	fmt.Println(string(res))

	if err != nil {
		return err
	}

	writer := bufio.NewWriter(file)

	_, err = writer.Write(res)

	if err != nil {
		return err
	}

	err = writer.Flush()

	if err != nil {
		return err
	}

	return nil
}

func (x *_xml) Read(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	scanner.Scan()

	text := scanner.Text()

	err = xml.Unmarshal([]byte(text), x)

	if err != nil {
		return err
	}

	x.String()

	return nil
}

func (x *_xml) Update(path string) error {
	file, err := os.OpenFile(path, os.O_RDWR, 0755)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(file)

	scanner.Scan()

	text := scanner.Text()

	err = xml.Unmarshal([]byte(text), x)

	if err != nil {
		return err
	}

	x.String()

	fmt.Println("Введите через запятую следующие поля и значения, через запятую, которые надо изменить [поле=значение,поле=значение]: ")

	scanner = bufio.NewScanner(os.Stdin)
	scanner.Scan()
	data := strings.Split(scanner.Text(), ",")

	for i := range data {
		arr := strings.Split(data[i], "=")
		switch arr[0] {
		case "Fio":
			x.Fio = arr[1]
		case "Year":
			x.Year = arr[1]
		case "Facult":
			x.Facult = arr[1]
		default:
			return errors.New("Error: Not found this field.")
		}
	}

	err = file.Close()

	if err != nil {
		return nil
	}

	file, err = os.Create(path)
	if err != nil {
		return nil
	}
	defer file.Close()

	res, err := xml.Marshal(x)

	if err != nil {
		return nil
	}

	_, err = file.Write(res)

	if err != nil {
		return err
	}

	return err
}

func (x *_xml) Delete(path string) error {
	err := os.Remove(path)
	return err
}

func (x *_xml) String() {
	fmt.Println("Fio: ", x.Fio)
	fmt.Println("Year: ", x.Year)
	fmt.Println("Facult: ", x.Facult)
}

package files

type file struct {
	path string
	filer
}

func NewFile(path, _type string) *file {
	file := &file{
		path:  path,
		filer: nil,
	}

	switch _type {
	case "json":
		file.filer = NewJson()
	case "xml":
		file.filer = NewXml()
	case "txt":
		file.filer = NewTxt()
	case "zip":
		file.filer = NewZip()
	}

	return file
}

type filer interface {
	Create(path string) error
	Read(path string) error
	Update(path string) error
	Delete(path string) error
}

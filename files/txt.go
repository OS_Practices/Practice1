package files

import (
	"bufio"
	"fmt"
	"os"
)

type _txt struct {
	Text string
}

func NewTxt() *_txt {
	return &_txt{
		Text: "",
	}
}

func (t *_txt) Create(path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	fmt.Println("Пожалуйста, введите текст, который надо записать в новый файл:")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()

	t.Text = scanner.Text()

	writer := bufio.NewWriter(file)

	_, err = writer.WriteString(t.Text)

	if err != nil {
		return err
	}

	err = writer.Flush()

	if err != nil {
		return err
	}

	return nil
}

func (t *_txt) Read(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	fmt.Println(scanner.Text())

	return nil
}

func (t *_txt) Update(path string) error {
	file, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND, 0755)
	if err != nil {
		return err
	}
	defer file.Close()

	fmt.Println("Введите текст, который надо добавить в файл:")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()

	writer := bufio.NewWriter(file)
	_, err = writer.WriteString(scanner.Text())

	if err != nil {
		return err
	}

	err = writer.Flush()

	return err
}

func (t *_txt) Delete(path string) error {
	err := os.Remove(path)
	return err
}

func (t *_txt) String() {
	fmt.Println(t.Text)
}

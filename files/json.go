package files

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
)

type _json struct {
	Fio    string `json:"fio"`
	Year   string `json:"year"`
	Facult string `json:"facult"`
}

func NewJson() *_json {
	return &_json{
		Fio:    "none",
		Year:   "none",
		Facult: "none",
	}
}

func (j *_json) Create(path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	fmt.Println("Пожалуйста, введите информацию о ФИО, годе поступления и направлении студента, через запятую:")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	data := strings.Split(scanner.Text(), ",")

	j.Fio = data[0]
	j.Year = data[1]
	j.Facult = data[2]

	res, err := json.Marshal(j)

	fmt.Println(string(res))

	if err != nil {
		return err
	}

	writer := bufio.NewWriter(file)

	_, err = writer.Write(res)

	if err != nil {
		return err
	}

	err = writer.Flush()

	if err != nil {
		return err
	}

	return nil
}

func (j *_json) Read(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	scanner.Scan()

	text := scanner.Text()

	err = json.Unmarshal([]byte(text), j)

	if err != nil {
		return err
	}

	j.String()

	return nil
}

func (j *_json) Update(path string) error {
	file, err := os.OpenFile(path, os.O_RDWR, 0755)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(file)

	scanner.Scan()

	text := scanner.Text()

	err = json.Unmarshal([]byte(text), j)

	if err != nil {
		return err
	}

	j.String()

	fmt.Println("Введите через запятую следующие поля и значения, через запятую, которые надо изменить [поле=значение,поле=значение]: ")

	scanner = bufio.NewScanner(os.Stdin)
	scanner.Scan()
	data := strings.Split(scanner.Text(), ",")

	for i := range data {
		arr := strings.Split(data[i], "=")
		switch arr[0] {
		case "Fio":
			j.Fio = arr[1]
		case "Year":
			j.Year = arr[1]
		case "Facult":
			j.Facult = arr[1]
		default:
			return errors.New("Error: Not found this field.")
		}
	}

	err = file.Close()

	if err != nil {
		return nil
	}

	file, err = os.Create(path)
	if err != nil {
		return nil
	}
	defer file.Close()

	res, err := json.Marshal(j)

	if err != nil {
		return nil
	}

	_, err = file.Write(res)

	if err != nil {
		return err
	}

	return err
}

func (j *_json) Delete(path string) error {
	err := os.Remove(path)
	return err
}

func (j *_json) String() {
	fmt.Println("Fio: ", j.Fio)
	fmt.Println("Year: ", j.Year)
	fmt.Println("Facult: ", j.Facult)
}

package main

import (
	"OS_Practice1/files"
	"errors"
	"flag"
	"fmt"
	"os"
)

type Commander interface {
	Init([]string) error
	Run() error
}

type diskCommand struct {
	fs *flag.FlagSet
}

func NewDiskCommand() *diskCommand {
	disk := &diskCommand{
		fs: flag.NewFlagSet("disk", flag.ContinueOnError),
	}
	return disk
}

func (disk *diskCommand) Init(args []string) error {
	disk.fs.Usage = func() {
		fmt.Printf("Programg %s print info about mount points.\n", os.Args[1])
	}
	err := disk.fs.Parse(args)
	return err
}

func (disk *diskCommand) Run() error {
	err := GetMountsInfo()
	return err
}

type fileCommand struct {
	fs      *flag.FlagSet
	_type   string
	command string
	path    string
}

func NewFileCommand() *fileCommand {
	file := &fileCommand{
		fs:      flag.NewFlagSet("file", flag.ContinueOnError),
		command: "none",
		_type:   "none",
		path:    "none",
	}

	return file
}

func (file *fileCommand) Init(args []string) error {
	file.fs.Usage = func() {
		fmt.Printf("Program %s make action with files.\n", os.Args[1])
		fmt.Printf("Program syntax: %s -t [type file] -c [action with file] [path to file]\n", os.Args[1])
		file.fs.PrintDefaults()
	}
	file.fs.StringVar(&file._type, "t", "txt", "This flag define file type. [json,txt,xml,zip]")
	file.fs.StringVar(&file.command, "c", "read", "This flag define action with file. [create,read,delete]")
	err := file.fs.Parse(args)
	file.path = file.fs.Arg(0)
	return err
}

func (file *fileCommand) Run() error {
	fl := files.NewFile(file.path, file._type)
	var err error

	switch file.command {
	case "create":
		err = fl.Create(file.path)

	case "read":
		err = fl.Read(file.path)

	case "update":
		err = fl.Update(file.path)

	case "delete":
		err = os.Remove(file.path)

	default:
		err = errors.New("Error: Unknown action with file")
	}

	return err
}

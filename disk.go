package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"syscall"
)

func GetMountsInfo() error {
	statfs := &syscall.Statfs_t{}
	lines, err := getMounts()
	fsTypes := getSystemFs()
	if err != nil && err != io.EOF {
		log.Fatal(err)
	}

	for _, line := range lines {
		mounts := strings.Fields(line)

		if contains(mounts[2], fsTypes) {
			err = syscall.Statfs(mounts[1], statfs)

			if err != nil {
				return err
			}

			fmt.Printf(`MountPoint: %s
MountDir: %s
FileSystem: %s
SizeMountPointInBits: %d

`, mounts[0], mounts[1], mounts[2], statfs.Blocks*uint64(statfs.Bsize))
		}
	}

	return nil
}

func getMounts() ([]string, error) {
	mounts, err := os.Open("/proc/1/mounts")
	if err != nil {
		return nil, err
	}
	defer mounts.Close()

	lines := make([]string, 0, 0)
	reader := bufio.NewReader(mounts)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF && len(line) > 0 {
				lines = append(lines, strings.TrimSpace(line))
			}
			return lines, err
		}
		lines = append(lines, strings.TrimSpace(line))
	}

	return lines, nil
}

func getSystemFs() []string {
	fsFile, err := os.Open("/proc/filesystems")
	if err != nil {
		return nil
	}
	defer fsFile.Close()

	reader := bufio.NewReader(fsFile)
	lines := make([]string, 0, 0)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF && len(line) > 0 && strings.Fields(line)[0] != "nodev" {
				lines = append(lines, strings.TrimSpace(line))
			}
			return lines
		}

		if strings.Fields(line)[0] != "nodev" {
			lines = append(lines, strings.TrimSpace(line))
		}
	}
	return lines
}

func contains(val string, arr []string) bool {
	for _, elem := range arr {
		if val == elem {
			return true
		}
	}

	return false
}

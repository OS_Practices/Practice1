package main

import (
	"log"
	"os"
)

var (
	command       string
	filePath      string
	operationType string
)

func main() {
	if len(os.Args) <= 1 {
		log.Fatal("Error: Program doesn`t have subcommand. [file, disk]")
	}

	cmds := map[string]Commander{
		"disk": NewDiskCommand(),
		"file": NewFileCommand(),
	}

	subcommand := os.Args[1]

	if _, ok := cmds[subcommand]; !ok {
		log.Fatal("Error: This command doesn`t exist. [file, disk]")
	}

	err := cmds[subcommand].Init(os.Args[2:])

	if err != nil {
		log.Fatal(err)
	}

	err = cmds[subcommand].Run()
	if err != nil {
		log.Fatal(err)
	}
}
